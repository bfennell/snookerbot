# README #

Telegram BOT that reads Snooker event/match/player information from api.snooker.org and sends it to Telegram chats.

# Usage #

Two environment variables need to be setup  before runnning:

export SNOOKERBOT_TOKEN='your-token'
export SNOOKERBOT_REQBY='your-id-string'

To launch simply run:

go run main.go