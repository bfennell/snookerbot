// ===-----------------------------------------------------------------------===
// Telegram BOT that reads Snooker event/match/player information from
// api.snooker.org and sends it to Telegram chats.
//
// Usage
// > export SNOOKERBOT_TOKEN='your-token'
// > export SNOOKERBOT_REQBY='your-id-string'
// > go run main.go
// ===-----------------------------------------------------------------------===
package main

import (
	"os"
	"fmt"
	"sort"
	"time"
	"log"
	"regexp"
	"strconv"
	"io/ioutil"
	"net/http"
	"encoding/json"
	"github.com/rockneurotiko/go-tgbot"
)

// api.snooker.org would like some identification
var SNOOKERBOT_REQBY string = "DefaultSnookerTelegramBot"

// ===-----------------------------------------------------------------------===
// Logging
// ===-----------------------------------------------------------------------===

var LOG *log.Logger

// ===-----------------------------------------------------------------------===
// Helpers
// ===-----------------------------------------------------------------------===

func check_error (e error) {
    if e != nil {
        panic (e)
    }
}

func appendIf(slice []string, i string) []string {
    for _, ele := range slice {
        if ele == i {
            return slice
        }
    }
    return append(slice, i)
}

func parse_date(date string) string {
	re := regexp.MustCompile("^(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)")
	result := re.FindAllStringSubmatch(date, -1)
	if len(result) != 1 && len(result[0]) != 4 {
		return "<unknown date format>"
	}
	year,_  := strconv.Atoi(result[0][1])
	month,_ := strconv.Atoi(result[0][2])
	day,_   := strconv.Atoi(result[0][3])
	t := time.Date(year,time.Month(month),day,0,0,0,0, time.UTC)
	s := fmt.Sprintf("%d %s %d", t.Day(), t.Month(), t.Year())
	return s
}

func get_hour_minute(date string) string {
	// "2015-12-13T10:08:45Z"
	timeRe := regexp.MustCompile("^(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)T(\\d+:\\d+):\\d+Z")

	hour_minute := date
	result := timeRe.FindAllStringSubmatch(date, -1)
	if len(result) == 1 && len(result[0]) == 5 {
		hour_minute = result[0][4]
	}
	return hour_minute
}

// return true if the path exists otherwise false
func path_exists (path string) bool {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return false
		} else {
			check_error (err)
		}
	}
	return true
}

// create the season dir if it does not exist
func create_season_dir (year int) string {
	dir := fmt.Sprintf("%d_%d", year, year+1)
	if !path_exists(dir) {
		err := os.Mkdir(dir, 0755)
		check_error(err)
	}
	return dir
}

// ===-----------------------------------------------------------------------===
// api.snooker.org
// ===-----------------------------------------------------------------------===

func get_url (filename string, url string) {
	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	check_error (err)

	req.Header.Set("X-Requested-By", SNOOKERBOT_REQBY)

	resp, err := client.Do(req)
	check_error (err)

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	check_error (err)

	err = ioutil.WriteFile(filename, body, 0644)
	check_error (err)
}

// Get all events of a season: 2015 means season 2015=>2016
func get_events_season (year int, refresh bool) string {
	season_dir := create_season_dir (year)
	filename := fmt.Sprintf("%s/events.json", season_dir)
	if refresh || !path_exists(filename) {
		LOG.Printf("%s does not exist, fetching", filename)
		// URL format: api.snooker.org/?t=5&s=[Season]
		get_url (filename, fmt.Sprintf("http://api.snooker.org/?t=5&s=%d", year));
	}
	return filename
}

// Get all matches of an event
func get_events_matches (year int, eventId int, refresh bool) string {
	season_dir := create_season_dir (year)
	filename := fmt.Sprintf("%s/matches_%d.json", season_dir, eventId)
	if refresh || !path_exists(filename) {
		LOG.Printf("%s does not exist, fetching", filename)
		// URL format: api.snooker.org/?t=6&e=[Event ID]
		get_url (filename, fmt.Sprintf("http://api.snooker.org/?t=6&e=%d", eventId));
	}
	return filename
}

// Get players of an event
func get_events_players (year int, eventId int) string {
	season_dir := create_season_dir (year)
	filename := fmt.Sprintf("%s/players_%d.json", season_dir, eventId)
	if !path_exists(filename) {
		LOG.Printf("%s does not exist, fetching", filename)
		// URL format: api.snooker.org/?t=9&e=[Event ID]
		get_url (filename, fmt.Sprintf("http://api.snooker.org/?t=9&e=%d", eventId));
	}
	return filename
}

// ===-----------------------------------------------------------------------===
// Player
// ===-----------------------------------------------------------------------===
type Player struct {
	ID          int
	FirstName   string
	LastName    string
	Nationality string
}

type Players []Player;

func find_player(year int, eventId int, playerId int) string {
	file := get_events_players(year,eventId)

	content, err := ioutil.ReadFile(file)
	check_error (err)

	var players Players;
	json.Unmarshal(content, &players)

	for i := range players {
		if players[i].ID == playerId {
			return fmt.Sprintf("%s %s", players[i].FirstName, players[i].LastName)
		}
	}

	return "<Unknown Player>"
}

// ===-----------------------------------------------------------------------===
// Match
// ===-----------------------------------------------------------------------===
type Match struct {
	ID          int
	EventId     int
	Round       int
	Player1ID   int
	Player2ID   int
	Score1      int
	Score2      int
	FrameScores   string
	StartDate     string
	EndDate       string
	ScheduledDate string
	VideoURL      string
	Unfinished    bool
}

type Matches []Match;
func (slice Matches) Len() int            { return len(slice)                               }
func (slice Matches) Less (i, j int) bool { return slice[i].EndDate < slice[j].EndDate      }
func (slice Matches) Swap(i, j int)       { slice[i], slice[j] = slice[j], slice[i]         }

func find_matches (time time.Time, year int, eventId int, refresh bool) (string, []string) {
	date := fmt.Sprintf("%d-%02d-%02d", time.Year(), time.Month(), time.Day())

	file := get_events_matches(year,eventId,refresh)

	content, err := ioutil.ReadFile(file)
	check_error (err)

	var matches Matches;
	json.Unmarshal(content, &matches)
	sort.Sort(matches)

	playerRe := regexp.MustCompile("TBD")

	var videos []string
	var completed string
	var ongoing string
	found := false
	for i := range matches {
		if len(matches[i].ScheduledDate) >= len(date) {
			sdate := matches[i].ScheduledDate[:len(date)]
			if sdate == date {
				p1 := find_player(time.Year(), eventId, matches[i].Player1ID)
				p2 := find_player(time.Year(), eventId, matches[i].Player2ID)
				if nil == playerRe.FindAllString(p1,-1) && nil == playerRe.FindAllString(p2,-1) {
					if matches[i].Unfinished || (matches[i].Score1 == 0 && matches[i].Score2 == 0) {
						startTime := get_hour_minute(matches[i].ScheduledDate)
						ongoing += fmt.Sprintf("%s %s vs %s (%d-%d)\n", startTime, p1, p2, matches[i].Score1, matches[i].Score2)
					} else {
						endTime := get_hour_minute(matches[i].EndDate)
						completed += fmt.Sprintf("%s %s vs %s (%d-%d)\n", endTime, p1, p2, matches[i].Score1, matches[i].Score2)
					}
					if len(matches[i].VideoURL) != 0 {
						videos = appendIf(videos, matches[i].VideoURL)
					}
					found = true
				}
			}
		}
	}
	var str string
	if !found { str = "No matches today" }
	if len(ongoing) != 0 { str += "-------\n" + ongoing }
	if len(completed) != 0 { str += "--- completed today ---\n" + completed }

	LOG.Printf("match search returned => %s", str)
	return str,videos
}

// ===-----------------------------------------------------------------------===
// Event
// ===-----------------------------------------------------------------------===
type Event struct {
	ID        int
	Name      string
	StartDate string
	EndDate   string
	Country   string
}

type Events []Event;
func (slice Events) Len() int            { return len(slice)                               }
func (slice Events) Less (i, j int) bool { return slice[i].StartDate < slice[j].StartDate  }
func (slice Events) Swap(i, j int)       { slice[i], slice[j] = slice[j], slice[i]         }

func find_event (time time.Time, refresh bool) (string, []string) {
	date := fmt.Sprintf("%d-%02d-%02d", time.Year(), time.Month(), time.Day())

	// search now.year and now.year-1 as seasons straddle two years
	files := [2]string {get_events_season (time.Year()-1,refresh), get_events_season (time.Year(),refresh)}

	var videos []string
	var retStr string
	for _, file := range files {
		content, err := ioutil.ReadFile(file)
		check_error (err)

		var events Events;
		json.Unmarshal(content, &events)
		sort.Sort(events)

	loop:
		for i := range events {
			// find an event running during "date"
			if ((events[i].StartDate <= date) && (events[i].EndDate >= date)) {
				var mstr string
				retStr = fmt.Sprintf("%s - %s to %s\n",events[i].Name, parse_date(events[i].StartDate), parse_date(events[i].EndDate))
				LOG.Printf("found event %s", retStr)
				mstr,videos = find_matches (time, time.Year(), events[i].ID, refresh)
				retStr += mstr
				break loop
			}
			// find the next event
			if events[i].StartDate > date {
				retStr = fmt.Sprintf("Next Event: %s - %s to %s\n",events[i].Name, parse_date(events[i].StartDate), parse_date(events[i].EndDate))
				break loop
			}
		}
	}

	return retStr, videos
}

// ===-----------------------------------------------------------------------===
// Log all messages to the log file
// ===-----------------------------------------------------------------------===

func allMsgHand (bot tgbot.TgBot, msg tgbot.Message) {
	LOG.Printf("Received message: %+v\n", msg)
}

// ===-----------------------------------------------------------------------===
// help : Command
// ===-----------------------------------------------------------------------===

func help(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
	answertext := `/today - get scores for today's matches`
	return &answertext
}

// ===-----------------------------------------------------------------------===
// today : Command
// ===-----------------------------------------------------------------------===
func today(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
	txt,videos := find_event (time.Now(), false)
	for _,v := range videos {
		bot.SimpleSendMessage(msg, v)
	}
	return &txt
}

func main() {
	if len(os.ExpandEnv("$SNOOKERBOT_REQBY")) != 0 {
		SNOOKERBOT_REQBY = os.ExpandEnv("$SNOOKERBOT_REQBY")
	}

	// Create the log file
	logfile, err := os.OpenFile("snookerbot.log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	check_error(err)
	defer logfile.Close()
	LOG = log.New(logfile, "logger: ", log.Lshortfile)
	LOG.Printf("%s ====> Started <====", time.Now().Format(time.UnixDate))

	// refresh events on start
	find_event(time.Now(), true)

	// Start a timer to periodically retrieve scores
	ticker := time.NewTicker(30 * 60 * time.Second)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <- ticker.C:
				// update event, player & score information
				LOG.Printf("%s ----> Update <----", time.Now().Format(time.UnixDate))
				find_event(time.Now(), true)
			case <- quit:
				ticker.Stop()
				return
			}
		}
	}()

	token := os.ExpandEnv("$SNOOKERBOT_TOKEN")
	if len(token) != 0 {
		bot := tgbot.New(token)
		bot.SimpleCommandFn(`help`, help)
		bot.SimpleCommandFn(`today`, today)
		bot.AnyMsgFn(allMsgHand)
		bot.SimpleStart()
	} else {
		fmt.Printf("Error: environment variable SNOOKERBOT_TOKEN is not set, exiting....\n")
	}
}
